Rails.application.routes.draw do
  devise_for :users
  root 'users#index'

  resources :articles do
    resources :comments
  end

  # get "/users", to: 'users#index'
  # get '/users/:id', to: 'articles#index'

  resources :users
end
