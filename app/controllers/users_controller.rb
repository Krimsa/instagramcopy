class UsersController < ApplicationController
  def index
    @users = User.all
  end

  def show
    @articles = User.find(params[:id]).articles
    @user_display = User.find(params[:id])
  end
end
