class Article < ApplicationRecord
  # include Visible
  mount_uploader :photo, PhotoUploader

  belongs_to :user
  has_many :comments, dependent: :destroy

  validates :title, presence: true
  validates :body, presence: true, length: { minimum: 5 }
end
