module Visible
  extend ActiveSupport::Concern
  class_methods do
    def public_count
      count
    end
  end
end
